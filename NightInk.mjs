"use strict";

import fs from 'fs';
import { encode } from 'html-entities';

function locate_part(data, parts) {
	let sub_data = data;
	for(let part of parts) {
		if(sub_data == null || typeof sub_data != "object")
			return null;
		
		sub_data = sub_data[part];
	}
	return sub_data;
}

async function NightInkFile(filename, data) {
	return NightInk(
		await fs.promises.readFile(filename, "utf8"),
		data
	);
}

function NightInk(template, data) {
	return template.replace(
			/\{#each\s+[^}]+\}([^]*?)\{#endeach\}/gim,
			function(substr, index, template) {
				let inner_template = substr.substring(
					substr.indexOf("}") + 1,
					substr.length - "{#endeach}".length
				)
				
				let parts = substr.match(/^\{#each\s+([^}]+)\}/)[1]
					.split(".");
				let sub_data = locate_part(data, parts);
				
				let result = "";
				for(let item of sub_data)
					result += NightInk(inner_template, item);
				return result;
			}
		)
		.replace(
			/\{\{?[^}]*\}\}?/gi,
			function(substr, index, template) {
				// Find the value to replace it with
				let parts = substr.replace(/[{}]/g, "").split(".").filter((p) => p.length > 0);
				
				// Process & encode with HTML entities if required
				let sub_data = parts.length > 0 ? locate_part(data, parts) : data;
				sub_data = sub_data == null ? "" : sub_data.toString();
				
				if(substr.startsWith("{{"))
					sub_data = encode(sub_data);
				// Perform replacement
				return sub_data.toString();
			}
		)
}

export default NightInk;
export { NightInk, NightInkFile };
