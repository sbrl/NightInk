# Changelog

Release template text:

-----

Install or update from npm:

```bash
npm install --save nightink
```

-----


## v1.0.2 - 14th June 2022
 - Allow multiple `{#each thing}...{#endeach}` blocks in a single template string / file


## v1.0.1 - 10th March 2022
 - Fix crash in html entities encoder


## v1.0 - 5th March 2022
 - I should have bumped it to 1.0 aaages ago
 - Update dependencies
 - Remove `await-fs` dependency since `fs.promises` is now a thing


## v0.1.1 - 27th August 2018
 - Specify npm dependencies


## v0.1 - 27th August 2018
 - Initial release!
