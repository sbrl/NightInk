# NightInk

> A teeny-tiny templating engine.

NightInk is a teeny-tiny Javascript templating engine, written as an ES6 module. While there are loads of existing templating engines out there, I couldn't find one I liked (anyone know of a comparison site? :P) - so I wrote my own (it's adding to the problem, I know!).

Also available in PHP: [NightInk.php](https://gist.github.com/710903717a721a89486f365b799e5bb1#file-nightink-php)

## Installation
Installation is done through `npm`:

```bash
npm install --save nightink
```

## Usage
Here's a basic example:

```js
import { NightInk } from '../NightInk.mjs';

// .....

let html = NightInk(template_string, {
	// options here
})
```

The contents of the options object is substituted into the template string. Consider the following template:

```html
<p>Next launch in {{seconds}}!</p>
<p>Rocket info: {{model}} carrying {{payload.summary}} for {{payload.owner.name}}, totalling {{totals.weight}} tons</p>
```

...with this options object:

```js
{
	seconds: 10,
	model: "Ariane 6",
	payload: {
		summary: "Sentinel-4",
		owner: {
			name: "Dr. Sean",
			company: "Sean's Satellites Inc."
		}
	},
	totals: {
		weight: 450,
		tomatoes: 56
	}
}
```

All the appropriate values will get extracted from the options object and substituted into the template string automagically. Under normal circumstances, values are escaped with HTML-entities before substitution (if appropriate), for security purposes (no script-injection attacks here, thank you!). If you'd like to disable this (make sure you know what you're doing!), simply use a single set of curly braces:

```html
<p>The rocket escaped: {{escape_me}}</p>
<p>The launchpad did not escape {no_escaping_here}</p>
```

_NightInk_ also has support for arrays (multiple levelled nested arrays are ok too :D). This is done like so:

```html
<p>To go to {{place_name}}, you will need:</p> 
<ul>
	{#each list}
	<li>{{quantity}} x {{name}}</li>
	{#endeach}
</ul>
```

```js
{
	plane_name: "space"
	list: [
		{ name: "Rocket", quantity: 1 },
		{ name: "Astronaut", quantity: 3 },
		{ name: "Fuel Cells", quantity: 55 },
		{ name: "Launchpad", quantity: 1 }
	]
}
```

## License
_NightInk_ is licensed under the _Mozilla Public Licence 2.0_. A copy of this license can be found in the `LICENSE` file in this repository.

Alternatively, tldr-legal [has a summary](https://tldrlegal.com/license/mozilla-public-license-2.0-%28mpl-2%29)that you may find useful.
